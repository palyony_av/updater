import http
import sqlite3
import os

DB_FILENAME = "info.db"
TABLE_UPDATE_INFO = "update_info"

## Predefined values
# Netfilter module
NET_NAME = "network"
NET_VERSION = 1
NET_PATH = "E:\\PalyonyAV\\lib\\net-filter-dll.dll"
# Cloud Scan module
CLOUD_NAME = "cloud"
CLOUD_VERSION = 1
CLOUD_PATH = "E:\\PalyonyAV\\lib\\CuckooSandboxDll.dll"
# Logger module
LOG_NAME = "log"
LOG_VERSION = 1
LOG_PATH = "E:\\PalyonyAV\\lib\\Logger.dll"


def get_module_version(module_name: str) -> None or str:
    connection = sqlite3.connect(DB_FILENAME)
    cursor = connection.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{}'".format(TABLE_UPDATE_INFO))
    if cursor.fetchone() is None:
        return None
    
    cursor.execute("SELECT version FROM {} WHERE name = '{}'".format(TABLE_UPDATE_INFO, module_name))
    query = cursor.fetchone()
    if query is None:
        return

    version = query[0]
    if version is not None:
        version = str(version)

    connection.close()
    return version

def get_module_path(module_name: str) -> None or str:
    connection = sqlite3.connect(DB_FILENAME)
    cursor = connection.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{}'".format(TABLE_UPDATE_INFO))
    if cursor.fetchone() is None:
        return None
    
    cursor.execute("SELECT module_path FROM {} WHERE name = '{}'".format(TABLE_UPDATE_INFO, module_name))
    module_path = cursor.fetchone()[0]
    print("The module name is: ", module_name)
    print(module_path)

    connection.close()
    return module_path

# def init_db():
#     connection = sqlite3.connect(DB_FILENAME)
#     cursor = connection.cursor()
#     cursor.execute("SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{}'".format(TABLE_UPDATE_INFO))
#     if cursor.fetchone() is None:
#         cursor.execute(
#             "CREATE TABLE {} ( name VARCHAR(10) PRIMARY KEY, version INTEGER, module_path VARCHAR(255) )".format(
#                 TABLE_UPDATE_INFO
#             )
#         )
#         connection.commit()
#         cursor.execute(
#             "INSERT INTO {} (name, version, module_path) VALUES ('{}', '{}', '{}')".format(
#                 TABLE_UPDATE_INFO, NET_NAME, NET_VERSION, NET_PATH
#             )
#         )
#         connection.commit()
#         cursor.execute(
#             "INSERT INTO {} (name, version, module_path) VALUES ('{}', '{}', '{}')".format(
#                 TABLE_UPDATE_INFO, CLOUD_NAME, CLOUD_VERSION, CLOUD_PATH
#             )
#         )
#         connection.commit()
#         cursor.execute(
#             "INSERT INTO {} (name, version, module_path) VALUES ('{}', '{}', '{}')".format(
#                 TABLE_UPDATE_INFO, LOG_NAME, LOG_VERSION, LOG_PATH
#             )
#         )
#         connection.commit()

class HttpReuqestHandler(http.server.BaseHTTPRequestHandler):
    def _set_headers(self, error_code: int = 200, content_type = "text/html"):
        self.send_response(error_code)
        # self.send_header("Content-type", "application/x-www-form-urlencoded")
        self.send_header("Content-type", content_type)
        self.end_headers()
    
    def do_GET(self):
        path_val = self.path.split('/')[1:]
        print(self.path)
        if len(path_val) < 2:
            self._set_headers(200)      # OK
            return

        if path_val[0] == "info":
            version = get_module_version(path_val[1])
            if version is not None:
                self._set_headers(200)  # OK
                self.wfile.write(version.encode("utf-8"))
            else:
                self._set_headers(500)  # Internal Server Error

        elif path_val[0] == "download":
            file_path = get_module_path(path_val[1])
            if file_path is not None:
                self._set_headers(200)  # OK
                with open(file_path, 'rb') as file:
                    self.wfile.write(file.read())
            else:
                self._set_headers(500)  # Internal Server Error
        else:
            self._set_headers(400)      # Bad Request


HOST = "0.0.0.0"
PORT = 8030

# init_db()

with http.server.HTTPServer((HOST, PORT), HttpReuqestHandler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()

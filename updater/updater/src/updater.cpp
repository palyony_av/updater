// This is an example which will explain the communication between
// the parent process (Master process) and a child process 
// (Slave process). The code makes a use of handle inheritance to pass
// the pipe's handle to the child process.

// The code has been developed on WIN-98 and tested on WIN98/2000.
// Code by Dinesh Ahuja, Aug 09, 2003.

/****************************************************************/
//						Compiler Settings
// Before executing the code, pass "-master" in the command line
// argument.
// Click on setting menu item, after that click on the Debug tab,
// type -master in the Program arguments: field.
/****************************************************************/

#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <tlhelp32.h>
#include <cstdlib>
#include <forward_list>
#include <memory>

#include "debug.h"

#define AV_UPDATER_GUI_MODULE_PATH "PalyonyAV.exe"
#define AV_UPDATE_FOLDER ".\\update\\"

// specifies the Buffer size for the pipe.
constexpr auto PIPE_BUFFERLENGTH = 19;
constexpr auto AV_UPDATER_REG_KEY = "SOFTWARE\\PalyonyAV\\Updater";
constexpr auto STR1 = "Hello World";

DWORD getParentPid()
{
	HANDLE hSnapshot = nullptr;
	PROCESSENTRY32 pe32;
	DWORD ppid = 0, pid = GetCurrentProcessId();

	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (hSnapshot == INVALID_HANDLE_VALUE)
	{
		goto Exit;
	}

	ZeroMemory(&pe32, sizeof(pe32));
	pe32.dwSize = sizeof(pe32);
	if (!Process32First(hSnapshot, &pe32))
	{
		goto Exit;
	}

	do
	{
		if (pe32.th32ProcessID == pid)
		{
			ppid = pe32.th32ParentProcessID;
			break;
		}
	} while (Process32Next(hSnapshot, &pe32));

Exit:
	if (hSnapshot != INVALID_HANDLE_VALUE && hSnapshot != nullptr)
	{
		CloseHandle(hSnapshot);
	}

	return ppid;
}

void HideConsole()
{
	::ShowWindow(::GetConsoleWindow(), SW_HIDE);
}

int main(int argc, char* argv[])
{
	HideConsole();

	BOOL bReturn = FALSE;

	Debug::log("Slave is started");

	// This code is executed for child process,
	HANDLE hRdSlave = nullptr;		// handle to read end to pipe2.
	unsigned long long ulladdRdSlave = 0;

	ulladdRdSlave = std::strtoull(argv[0], nullptr, 16);
	hRdSlave = reinterpret_cast<void*>(ulladdRdSlave);
	if (hRdSlave == nullptr)
	{
		Debug::log("The handle is null");
		return -1;
	}

	Debug::log("Slave is readed parent handle");

	DWORD parentPid = getParentPid();
	if (parentPid == 0)
	{
		return -1;
	}

	Debug::log("Get parent pid");

	HANDLE hParentProcess = OpenProcess(SYNCHRONIZE, FALSE, parentPid);
	if (hParentProcess == nullptr)
	{
		return -1;
	}

	Debug::log("Open parent process");

	PROCESS_INFORMATION prcInfo;
	STARTUPINFOA info;
	std::string fromPath;
	std::string toPath;
	/* Get number of read cycles */
	uint16_t readCycleNum;
	DWORD dwRead = sizeof(uint16_t);
	BOOL bRet;
	bRet = ReadFile((HANDLE)hRdSlave, &readCycleNum, sizeof(uint16_t), &dwRead, nullptr);
	if (bRet == FALSE)
	{
		return -1;
	}

	Debug::log("Read cycle num");

	std::forward_list<std::pair<char*, char*>> resultList;

	for (uint32_t i = 0; i < readCycleNum; i += 1)
	{
		uint16_t fromStrSize;
		char* szFromStr = nullptr;
		uint16_t toStrSize;
		char* szToStr = nullptr;

		Debug::log("Iterate");

		bRet = ReadFile((HANDLE)hRdSlave, &fromStrSize, sizeof(uint16_t), &dwRead, nullptr);
		if (bRet == FALSE)
		{
			goto ExitCycle;
		}

		szFromStr = new char[fromStrSize];
		bRet = ReadFile((HANDLE)hRdSlave, szFromStr, fromStrSize, &dwRead, nullptr);
		if (bRet == FALSE || fromStrSize != dwRead)
		{
			goto ExitCycle;
		}


		bRet = ReadFile((HANDLE)hRdSlave, &toStrSize, sizeof(uint16_t), &dwRead, nullptr);
		if (bRet == FALSE)
		{
			goto ExitCycle;
		}

		szToStr = new char[toStrSize];
		bRet = ReadFile((HANDLE)hRdSlave, szToStr, toStrSize, &dwRead, nullptr);
		if (bRet == FALSE || toStrSize != dwRead)
		{
			goto ExitCycle;
		}

		resultList.push_front(std::pair<char*, char*>(szFromStr, szToStr));

		continue;

	ExitCycle:
		if (szFromStr != nullptr)
		{
			delete[] szFromStr;
		}

		if (szToStr != nullptr)
		{
			delete[] szToStr;
		}
	}

	Debug::log("Wait while parent process exiting");
	WaitForSingleObject(hParentProcess, INFINITE);
	CloseHandle(hRdSlave);

	for (const auto& obj : resultList)
	{
		fromPath = AV_UPDATE_FOLDER + std::string(obj.first);
		toPath = ".\\" + std::string(obj.second);

		MoveFileExA(fromPath.c_str(), toPath.c_str(), MOVEFILE_REPLACE_EXISTING | MOVEFILE_WRITE_THROUGH);
	}

	Debug::log("All files are moved");

	GetStartupInfoA(&info);
	
	bReturn = CreateProcessA(AV_UPDATER_GUI_MODULE_PATH, nullptr, nullptr, nullptr, TRUE, NORMAL_PRIORITY_CLASS, nullptr, nullptr, &info, &prcInfo);
	if (bReturn == TRUE)
	{
		CloseHandle(prcInfo.hThread);
		CloseHandle(prcInfo.hProcess);
	}

	for (const auto& obj : resultList)
	{
		delete[] obj.first;
		delete[] obj.second;
	}

	Debug::log("All is done so exiting");

	return 0;
}

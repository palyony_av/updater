#include <Windows.h>
#include <cstdint>
#include <cstring>

#include "worker.h"
#include "curlWrapper.h"
#include "dll.h"

#define AV_UPDATER_REG_KEY "SOFTWARE\\PalyonyAV\\Updater"
#define AV_UPDATER_NETADDR "netaddr"
#define AV_UPDATER_MODULE_PATH "path"
#define AV_UPDATER_MODULE_VERSION "version"
#define AV_UPDATER_MODULE_STATE "state"
#define AV_UPDATER_DIRECTORY ".\\update\\"
#define REG_MAX_VALUE_SIZE 512
#define REG_MAX_KEY_NAME 128

enum class AVModuleState: DWORD
{
	updated = 0,
	downloaded = 1,
	waitForDownloading = 2
};

static bool getHKLMRegistryKeyHandle(const char* szSubKey, HKEY* pHKey);
static bool getCountSubkeys(HKEY hRootKey, PDWORD pCountSubKeys);
static bool getNetAddressOfKey(const char* szSubKey, char* valueBuffer, PDWORD pValueBufferSize);
static bool createUpdaterDirectory();

void task(void*)
{
	HKEY hKey = nullptr;
	DWORD cSubKeys = 0;
	unsigned int status = AV_STATUS_SUCCESS;
	char bufferKey[sizeof(AV_UPDATER_DIRECTORY) + REG_MAX_KEY_NAME];
	char* achKey = &bufferKey[sizeof(AV_UPDATER_DIRECTORY) - 1];
	char localVersionBuffer[REG_MAX_VALUE_SIZE];
	char remoteVersionBuffer[REG_MAX_VALUE_SIZE];
	char netaddr[REG_MAX_VALUE_SIZE];
	DWORD netaddrSize = REG_MAX_VALUE_SIZE;
	void* pCurl = initCurl();

	std::memcpy(bufferKey, AV_UPDATER_DIRECTORY, sizeof(AV_UPDATER_DIRECTORY));

	if (!getNetAddressOfKey(AV_UPDATER_REG_KEY, netaddr, &netaddrSize))
	{
		status = AV_STATUS_FAIL;
		goto Exit;
	}

	if (pCurl == nullptr)
	{
		status = AV_STATUS_FAIL;
		goto Exit;
	}

	if (!getHKLMRegistryKeyHandle(AV_UPDATER_REG_KEY, &hKey))
	{
		status = AV_STATUS_FAIL;
		goto Exit;
	}

	if (!getCountSubkeys(hKey, &cSubKeys))
	{
		status = AV_STATUS_FAIL;
		goto Exit;
	}

	for (DWORD i = 0; i < cSubKeys; i++)
	{
		DWORD retCode = ERROR_SUCCESS;
		DWORD cchKey = REG_MAX_KEY_NAME;
		DWORD cValueBuffer = REG_MAX_VALUE_SIZE;
		AVModuleState moduleState;
		DWORD moduleStateSize = sizeof(DWORD);
		HKEY hSubKey = nullptr;

		achKey[0] = L'\0';
		retCode = RegEnumKeyExA(
			hKey,							// A handle to an open registry key
			i,								// The index of the subkey to retrieve
			achKey,							// The name of the subkey
			&cchKey,						// The size of the buffer for the name of the subkey
			nullptr,						// Reserved and must be NULL
			nullptr,						// The user-defined class
			nullptr,						// The size of the buffer for the user-defined class
			nullptr							// A pointer to FILETIME structure
		);
		if (retCode != ERROR_SUCCESS)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		retCode = RegGetValueA(
			hKey,										// A handle to an open registry key
			achKey,										// The name of the registry key
			AV_UPDATER_MODULE_STATE,					// The name of the registry value
			RRF_RT_DWORD,								// The flags that restrict the data type of value to be queried
			nullptr,									// A code indicating the type of data stored in the specified value
			reinterpret_cast<DWORD *>(&moduleState),	// The value's data
			&moduleStateSize							// The size of the buffer for the value's data
		);
		if (retCode != ERROR_SUCCESS)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		if (moduleState == AVModuleState::downloaded)
		{
			continue;
		}

		localVersionBuffer[0] = L'\0';
		retCode = RegGetValueA(
			hKey,							// A handle to an open registry key
			achKey,							// The name of the registry key
			AV_UPDATER_MODULE_VERSION,		// The name of the registry value
			RRF_RT_REG_SZ,					// The flags that restrict the data type of value to be queried
			nullptr,						// A code indicating the type of data stored in the specified value
			localVersionBuffer,				// The value's data
			&cValueBuffer					// The size of the buffer for the value's data
		);
		if (retCode != ERROR_SUCCESS)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		remoteVersionBuffer[0] = '\0';
		if (getVersionInfo(pCurl, netaddr, achKey, remoteVersionBuffer) != HTTP_ERROR_OK)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		/* Nothing to update */
		if (!std::strcmp(remoteVersionBuffer, localVersionBuffer))
		{
			continue;
		}

		retCode = RegCreateKeyExA(
			hKey,								// A handle to an open registry key
			achKey,								// The name of a subkey
			0,									// Reserved and must be zero
			nullptr,							// The user-defined class type of this key
			0,									// Options
			KEY_ALL_ACCESS,						// The access rights for the key
			nullptr,							// A pointer to a SECURITY_ATTRIBUTES
			&hSubKey,							// Handle to the opened or created key
			nullptr								// Disposition information
		);
		if (retCode != ERROR_SUCCESS)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		moduleState = AVModuleState::waitForDownloading;
		retCode = RegSetValueExA(
			hSubKey,
			AV_UPDATER_MODULE_STATE,
			0,
			REG_DWORD,
			reinterpret_cast<const BYTE*>(&moduleState),
			sizeof(DWORD)
		);
		if (retCode != ERROR_SUCCESS)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		if (downloadFile(pCurl, netaddr, achKey, bufferKey) != HTTP_ERROR_OK)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		moduleState = AVModuleState::downloaded;
		retCode = RegSetValueExA(
			hSubKey,
			AV_UPDATER_MODULE_STATE,
			0,
			REG_DWORD,
			reinterpret_cast<const BYTE*>(&moduleState),
			sizeof(DWORD)
		);
		if (retCode != ERROR_SUCCESS)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		retCode = RegSetValueExA(
			hSubKey,
			AV_UPDATER_MODULE_VERSION,
			0,
			REG_SZ,
			reinterpret_cast<const BYTE*>(remoteVersionBuffer),
			std::strlen(remoteVersionBuffer) + 1
		);
		if (retCode != ERROR_SUCCESS)
		{
			status = AV_STATUS_FAIL;
			goto ExitCycle;
		}

		continue;

	ExitCycle:
		if (hSubKey != nullptr)
		{
			CloseHandle(hSubKey);
			hSubKey = nullptr;
		}

		goto Exit;
	}

Exit:
	if (hKey != nullptr)
	{
		CloseHandle(hKey);
	}
	if (pCurl != nullptr)
	{
		uninitCurl(&pCurl);
	}
}

UPDATERDLL_API AVHMODULE installModule()
{
	if (!createUpdaterDirectory())
	{
		return nullptr;
	}

	Worker* pWorker = Worker::Builder{}.setVectorFuncWork(std::vector<Worker::FuncWork>{task})
									   .build();
	if (!pWorker->waitForInit())
	{
		return nullptr;
	}

	return reinterpret_cast<void *>(pWorker);
}

UPDATERDLL_API unsigned int uninstallModule(AVHMODULE hModule)
{
	if (hModule == nullptr)
	{
		return AV_STATUS_FAIL;
	}

	delete static_cast<Worker*>(hModule);

	return AV_STATUS_SUCCESS;
}

UPDATERDLL_API unsigned int updateModuleData(AVHMODULE hModule)
{
	if (hModule == nullptr)
	{
		return AV_STATUS_FAIL;
	}

	Worker* pWorker = static_cast<Worker*>(hModule);

	/* Execute first and the only one work */
	pWorker->addNextWorkId(0);

	return AV_STATUS_SUCCESS;
}

bool getHKLMRegistryKeyHandle(const char *szSubKey, HKEY *pHKey)
{
	LSTATUS retCreateKeyCode;

	retCreateKeyCode = RegCreateKeyExA(
		HKEY_LOCAL_MACHINE,					// A handle to an open registry key
		szSubKey,							// The name of a subkey
		0,									// Reserved and must be zero
		nullptr,							// The user-defined class type of this key								
		0,									// Options
		KEY_ALL_ACCESS,						// The access rights for the key
		nullptr,							// A pointer to a SECURITY_ATTRIBUTES
		pHKey,								// Handle to the opened or created key
		nullptr								// Disposition information
	);

	return (retCreateKeyCode == ERROR_SUCCESS);
}

bool getCountSubkeys(HKEY hRootKey, PDWORD pCountSubKeys)
{
	LSTATUS retQueryInfoKeyCode;

	retQueryInfoKeyCode = RegQueryInfoKey(
		hRootKey,							// A handle to an open registry key
		nullptr,							// User-defined class of the key
		nullptr,							// Size of user-defined class of the key
		nullptr,							// Reserved and must be NULL
		pCountSubKeys,						// Number of subkeys
		nullptr,							// The size of the key's subkey with the longest name
		nullptr,							// The size of the longest string that specifies a subkey class
		nullptr,							// The number of values
		nullptr,							// The size of the key's longest value name
		nullptr,							// The size of the longest data component
		nullptr,							// The size of the key's security descriptor
		nullptr								// A pointer to a FILETIME
	);

	return (retQueryInfoKeyCode == ERROR_SUCCESS);
}

bool getNetAddressOfKey(const char* szSubKey, char *valueBuffer, PDWORD pValueBufferSize)
{
	DWORD retCode = RegGetValueA(
		HKEY_LOCAL_MACHINE,			// A handle to an open registry key
		szSubKey,					// The name of the registry key
		AV_UPDATER_NETADDR,			// The name of the registry value
		RRF_RT_REG_SZ,				// The flags that restrict the data type of value to be queried
		nullptr,					// A code indicating the type of data stored in the specified value
		valueBuffer,				// The value's data
		pValueBufferSize			// The size of the buffer for the value's data
	);

	return (retCode == ERROR_SUCCESS);
}

bool createUpdaterDirectory()
{
	BOOL result = CreateDirectoryExA(".", AV_UPDATER_DIRECTORY, nullptr);
	if (result == TRUE)
	{
		return true;
	} else {
		if (GetLastError() == ERROR_ALREADY_EXISTS)
		{
			return true;
		} else {
			return false;
		}
	}
}

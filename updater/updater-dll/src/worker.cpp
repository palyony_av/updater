#include "worker.h"

Worker::~Worker()
{
    Worker::kill();
}

Worker::Worker(Worker&& obj)
    :pThreadContext(std::move(obj.pThreadContext)),
        workThread(std::move(obj.workThread))
{

}

void Worker::addNextWorkId(uint32_t workId)
{
    return this->pThreadContext->addNextWorkId(workId);
}

bool Worker::waitForInit()
{
    return this->pThreadContext->waitForInit();
}

void Worker::kill()
{
    this->pThreadContext->kill();
    if (this->workThread.joinable())
    {
        this->workThread.join();
    }
}

Worker::Worker(std::vector<Worker::FuncWork> tableFuncWork, Worker::FuncInit funcInit, Worker::FuncWork funcUninit, Worker::FuncWork defaultWork)
    :pThreadContext(new ThreadContext(tableFuncWork, funcInit, funcUninit, defaultWork)),
    workThread(work, pThreadContext.get())
{

}

void Worker::work(Worker::ThreadContext *pContext)
{
    void *pData = pContext->init();

    while (pContext->isNeedToLive() && pData != nullptr)
    {
        pContext->setThreadState(Worker::ThreadContext::State::work);
        pContext->getNextWork()(pData);

        pContext->setThreadState(Worker::ThreadContext::State::wait);
        pContext->wait();
    }
    pContext->setThreadState(Worker::ThreadContext::State::uninit);
    pContext->uninit(pData);

    pContext->setThreadState(Worker::ThreadContext::State::die);
}

Worker::Builder& Worker::Builder::setVectorFuncWork(std::vector<Worker::FuncWork> inTableFuncWork)
{
    this->tableFuncWork = inTableFuncWork;
    return *this;
}

Worker::Builder& Worker::Builder::setFuncInit(FuncInit inFuncInit)
{
    this->funcInit = inFuncInit;
    return *this;
}

Worker::Builder& Worker::Builder::setFuncUnit(FuncWork inFuncUnit)
{
    this->funcUninit = inFuncUnit;
    return *this;
}

Worker::Builder& Worker::Builder::setDefaultWork(FuncWork inFuncWork)
{
    this->funcDefaultWork = inFuncWork;
    return *this;
}

Worker* Worker::Builder::build() const
{
    return new Worker{this->tableFuncWork, this->funcInit, this->funcUninit, this->funcDefaultWork};
}

Worker::ThreadContext::ThreadContext(std::vector<Worker::FuncWork> inWorkTable, Worker::FuncInit inFuncInit, 
                      Worker::FuncWork inFuncUninit, Worker::FuncWork inDefaultWork)
    :workTable(inWorkTable), state(Worker::ThreadContext::State::init)
{
    if (inFuncInit != nullptr)
    {
        this->init = inFuncInit;
    }

    if (inFuncUninit != nullptr)
    {
        this->uninit = inFuncUninit;
    }

    if (inDefaultWork != nullptr)
    {
        this->funcDefaultWork = inDefaultWork;
    }
}

Worker::FuncWork Worker::ThreadContext::getNextWork()
{
    std::unique_lock<std::mutex> lk(this->m);
    if (this->workQueue.empty())
    {
        return this->funcDefaultWork;
    }
    
    uint32_t workId = this->workQueue.front();
    this->workQueue.pop();

    if (workId < this->workTable.size())
    {
        return this->workTable[workId];
    } else {
        return funcDefaultWork;
    }
}

void Worker::ThreadContext::wait()
{
    using namespace std::chrono_literals;

    std::unique_lock<std::mutex> lk(this->m);
    if (this->alive == true && this->workQueue.empty())
    {
        cv.wait_for(lk, 5s);
    }
}

void Worker::ThreadContext::setThreadState(Worker::ThreadContext::State inState)
{
    std::unique_lock<std::mutex> lk(this->m);
    
    Worker::ThreadContext::State prevState = this->state;
    this->state = inState;

    lk.unlock();

    if (prevState == Worker::ThreadContext::State::init)
    {
        this->cv.notify_one();
    }
}

void Worker::ThreadContext::addNextWorkId(uint32_t workId)
{
    std::unique_lock<std::mutex> lk(this->m);
    this->workQueue.push(workId);
    lk.unlock();
    this->cv.notify_one();
}

void Worker::ThreadContext::kill()
{
    std::unique_lock<std::mutex> lk(this->m);
    this->alive = false;
    lk.unlock();
    this->cv.notify_one();
}

bool Worker::ThreadContext::isNeedToLive()
{
    std::unique_lock<std::mutex> lk(this->m);
    return this->alive;
}

/* ========= Default functions ========= */

void Worker::ThreadContext::defaultWork(void *)
{

}

void * Worker::ThreadContext::defaultInit()
{
    return reinterpret_cast<void *>(1);
}

void Worker::ThreadContext::defaultUninit(void *)
{

}

/* ------------------------------------- */

bool Worker::ThreadContext::waitForInit()
{
    using namespace std::chrono_literals;

    std::unique_lock<std::mutex> lk(this->m);
    if (this->state == Worker::ThreadContext::State::init)
    {
        this->cv.wait_for(lk, 10s);
    }

    if (this->state == Worker::ThreadContext::State::init)
    {
        return false;
    }

    if (this->state != Worker::ThreadContext::State::init
        && this->state != Worker::ThreadContext::State::die)
    {
        return true;        
    } else {
        return false;
    }
}

Worker::ThreadContext::State Worker::ThreadContext::getThreadState()
{
    std::unique_lock<std::mutex> lk(this->m);
    return this->state;
}

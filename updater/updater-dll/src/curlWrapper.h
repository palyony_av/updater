#ifndef __CURLWRAPPER_H__
#define __CURLWRAPPER_H__

#define HTTP_ERROR_OK 200

void* initCurl();
void uninitCurl(void** pInCurl);
long getVersionInfo(void* pInCurl, std::string szNetAddress, std::string szRemoteFileName, char* pServerResponseMessage);
long downloadFile(void* pInCurl, std::string szNetAddress, std::string szRemoteFileName, const char *szLocalFilename);

#endif // !__CURLWRAPPER_H__

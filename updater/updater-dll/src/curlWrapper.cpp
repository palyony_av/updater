#include <fstream>

#define CURL_STATICLIB
#include "curl/curl.h"

#pragma comment(lib, "libcurl_a.lib" )
#pragma comment(lib, "wldap32.lib" )
#pragma comment(lib, "crypt32.lib" )
#pragma comment(lib, "Ws2_32.lib" )
#pragma comment(lib, "Normaliz.lib" )

static size_t getVersionInfoCallback(void* pInBuf, size_t size, size_t nmemb, void* pUsrBuf);
static size_t downloadFileCallback(void* pInBuf, size_t size, size_t nmemb, void* pUsrBuf);

//#define VERSION_MAX_SIZE 10
//
//int main()
//{
//    CURL* pCurl = initCurl();
//    char buf[VERSION_MAX_SIZE + 1] = { 0 };
//    /*std::cout << getVersionInfo(pCurl, "http://127.0.0.1:8030/info/network", buf) << std::endl;*/
//    std::cout << downloadFile(pCurl, "http://127.0.0.1:8030/download/network", "netfilter-dll.dll") << std::endl;
//    std::cout << buf << std::endl;
//
//    uninitCurl(&pCurl);
//}

/* used in conjunction with sendGetRequest */
size_t downloadFileCallback(void* pInBuf, size_t size, size_t nmemb, void *pUsrFile)
{
    size_t totalSize = size * nmemb;
    std::ofstream* pFile = static_cast<std::ofstream*>(pUsrFile);

    pFile->write(static_cast<const char*>(pInBuf), totalSize);

    return totalSize;
}

long downloadFile(void* pInCurl, std::string szNetAddress, std::string szRemoteFileName, const char* szLocalFilename)
{
    CURL* pCurl = (CURL*)pInCurl;
    CURLcode retCode = CURLE_OK;
    long httpErrorCode = 0;
    curl_off_t downloadSize;
    std::ofstream file(szLocalFilename, std::ios::out | std::ios::binary);

    szNetAddress += "/download/" + szRemoteFileName;

    if (pCurl == nullptr || !file.is_open())
    {
        goto Exit;
    }

    /* reset all options of a libcurl session handle */
    curl_easy_reset(pInCurl);

    /* specify target */
    retCode = curl_easy_setopt(pCurl, CURLOPT_URL, szNetAddress.c_str());
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }
    curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, downloadFileCallback);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, &file);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    /* Now run off and do what you've been told! */
    retCode = curl_easy_perform(pCurl);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    retCode = curl_easy_getinfo(pCurl, CURLINFO_SIZE_DOWNLOAD_T, &downloadSize);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    retCode = curl_easy_getinfo(pCurl, CURLINFO_RESPONSE_CODE, &httpErrorCode);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

Exit:
    if (retCode != CURLE_OK || pCurl == NULL)
    {
        /* Return unknown 0 code */
        return 0;
    }
    return httpErrorCode;
}

/* used in conjunction with sendGetRequest */
size_t getVersionInfoCallback(void* pInBuf, size_t size, size_t nmemb, void* pUsrBuf)
{
    size_t totalSize = size * nmemb;

    std::memcpy(pUsrBuf, pInBuf, totalSize);

    return totalSize;
}

long getVersionInfo(void* pInCurl, std::string szNetAddress, std::string szRemoteFileName, char* pServerResponseMessage)
{
    CURL* pCurl = (CURL*)pInCurl;
    CURLcode retCode = CURLE_OK;
    long httpErrorCode;
    curl_off_t downloadSize;

    if (pCurl == NULL)
    {
        goto Exit;
    }

    szNetAddress += "/info/" + szRemoteFileName;

    /* reset all options of a libcurl session handle */
    curl_easy_reset(pInCurl);

    /* specify target */
    retCode = curl_easy_setopt(pCurl, CURLOPT_URL, szNetAddress.c_str());
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }
    curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, getVersionInfoCallback);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    if (pServerResponseMessage != NULL)
    {
        curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, pServerResponseMessage);
        if (retCode != CURLE_OK)
        {
            goto Exit;
        }
    }

    /* Now run off and do what you've been told! */
    retCode = curl_easy_perform(pCurl);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    retCode = curl_easy_getinfo(pCurl, CURLINFO_RESPONSE_CODE, &httpErrorCode);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

    retCode = curl_easy_getinfo(pCurl, CURLINFO_SIZE_DOWNLOAD_T, &downloadSize);
    if (retCode != CURLE_OK)
    {
        goto Exit;
    }

Exit:
    if (retCode != CURLE_OK || pCurl == NULL)
    {
        /* Return unknown 0 code */
        return 0;
    }
    return httpErrorCode;
}

void* initCurl()
{
    CURL* pCurl = NULL;

    /* In windows, this will init the winsock stuff */
    curl_global_init(CURL_GLOBAL_ALL);
    /* get a curl handle */
    pCurl = curl_easy_init();
    if (pCurl == NULL)
    {
        /* always cleanup */
        curl_easy_cleanup(pCurl);
        curl_global_cleanup();
        return NULL;
    }
    else {
        return pCurl;
    }
}

void uninitCurl(void** pInCurl)
{
    CURL* pCurl = (CURL*)*pInCurl;

    if (pCurl != NULL)
    {
        /* always cleanup */
        curl_easy_cleanup(pCurl);
        curl_global_cleanup();
        *pInCurl = NULL;
    }
}

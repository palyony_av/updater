#ifndef DLL_H
#define DLL_H

#ifdef UPDATERDLL_EXPORTS
#define UPDATERDLL_API __declspec(dllexport)
#else
#define UPDATERDLL_API __declspec(dllimport)
#endif // UPDATERDLL_EXPORTS

#define AV_STATUS_SUCCESS 1
#define AV_STATUS_FAIL 0

typedef void* AVHMODULE;

/* Installation preparations for module work */
extern "C" UPDATERDLL_API AVHMODULE installModule();

/* Release all module resources */
extern "C" UPDATERDLL_API unsigned int uninstallModule(AVHMODULE hModule);

/* Refresh all configs and tasks for module */
extern "C" UPDATERDLL_API unsigned int updateModuleData(AVHMODULE hModule);

#endif // !DLL_H
